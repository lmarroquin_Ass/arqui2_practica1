//-------------------
//  Arqui 2
//  Grupo 5
//  Estación Metereologica
//  1999-11130  Byron René Melgar Sajbin
//  2009-15030  Jorge Alberto Rodriguez Mayorga
//  2010-20210  ⁠⁠⁠⁠⁠Karen Melissa Lima Sandoval
//  2010-20952  Juan Luis Marroquín López
//  2010-21134  Dennis Fernando Higueros Ruano
//  2013-14655  Kristhal Jasmine Meng Marroquín
//-------------------- 

#include <SFE_BMP180.h>
#include <Wire.h>
#include <DHT11.h>
#include <SoftwareSerial.h>

int LOOP_DELAY = 1000;

// Pin  A5 and A4 are reserverde for  BMP180
int PIN_HUMO = A1;
int PIN_TEMP_MOIST = 2;
int PIN_PHOTO_RESISTOR=A0;
int PIN_RX_BLUETOOTH = 10;
int PIN_TX_BLUETOOTH = 11;

int humo;
int lightValue;
float temperatura, humedad; 
int status_DHT11;
double P,baseLine;
int statusBMP;
SoftwareSerial bluetooth(PIN_RX_BLUETOOTH,PIN_TX_BLUETOOTH); //RX,TX

DHT11 dht(PIN_TEMP_MOIST);
SFE_BMP180 pressure;

void checkHumo(){
  humo = analogRead(PIN_HUMO);
}

void checkTempMoistness(){
   status_DHT11  = dht.read(humedad,temperatura);
 }

void checkLight(){
    lightValue = analogRead(PIN_PHOTO_RESISTOR);
  }

void checkPressure(){
   char status;
   double T;
  status = pressure.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:
    delay(status);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Use '&T' to provide the address of T to the function.
    // Function returns 1 if successful, 0 if failure.

    status = pressure.getTemperature(T);
    if (status != 0)
    {
      // Start a pressure measurement:
      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      // If request is successful, the number of ms to wait is returned.
      // If request is unsuccessful, 0 is returned.

      status = pressure.startPressure(3);
      if (status != 0)
      {
        // Wait for the measurement to complete:
        delay(status);

        // Retrieve the completed pressure measurement:
        // Note that the measurement is stored in the variable P.
        // Use '&P' to provide the address of P.
        // Note also that the function requires the previous temperature measurement (T).
        // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
        // Function returns 1 if successful, 0 if failure.

        status = pressure.getPressure(P,T);
        if (status != 0)
        {
          statusBMP = 0;
        }
        else statusBMP =1 ;
      }
      else statusBMP =1;
    }
    else statusBMP = 2;
  }
  else statusBMP =2;
  }

void sendValues(){
      //String  smoke = "humo: "+ humo;
      bluetooth.print("humo: " );
      bluetooth.println(humo);
      bluetooth.print("Presion: ");
      bluetooth.println(P);
      bluetooth.print("Temperatura: " );
      bluetooth.println(temperatura);
      bluetooth.print("Humedad: ");
      bluetooth.println(humedad);
      bluetooth.print("Luz: ");
      bluetooth.println(lightValue);

      Serial.print("humo: " );
      Serial.println(humo);
      Serial.print("Presion: ");
      Serial.println(P);
      Serial.print("Temperatura: " );
      Serial.println(temperatura);
      Serial.print("Humedad: ");
      Serial.println(humedad);
      Serial.print("Luz: ");
      Serial.println(lightValue);
  }

void setup() {
  bluetooth.begin(9600);
  bluetooth.println("Bluetooth On...");

  Serial.begin(9600);
  if (pressure.begin())
    statusBMP = 0;
    
  else
  {
    // Oops, something went wrong, this is usually a connection problem,
    // see the comments at the top of this sketch for the proper connections.

    statusBMP =-1;
  }
  checkPressure();
  baseLine=P;
  
  
}

void loop() {
    checkHumo();
    checkTempMoistness();
   checkLight();
   checkPressure();
   sendValues();
   delay(LOOP_DELAY);
}
